public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DataSeatPrice dsp = new DataSeatPrice();
		TheaterManagement tm = new TheaterManagement(dsp.ticketPrices) ;
		
		System.out.println("1 : O 11 and O 12") ;
		tm.seat("O", 11) ;
		System.out.println("Ticket => O 11 : " + tm.getPrice()) ;
		tm.seat("O", 12) ;
		System.out.println("Ticket => O 12 : " + tm.getPrice() + "\n") ;

		System.out.println("2 : O 12 and O 13") ;
		tm.seat("O", 12) ;
		System.out.println("Ticket => O 12 : " + tm.getPrice()) ;
		tm.seat("O", 13) ;
		System.out.println("Ticket => O 13 : " + tm.getPrice() + "\n") ;
	}

}
