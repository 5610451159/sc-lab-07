public class TheaterManagement {
	int i ;
	int j ;
	int [][]ticket ;
	int price ;
	String p ;
	
	public TheaterManagement(int[][] ticketprice) {
		this.ticket = ticketprice ;
	}
	
	public void seat(String row, int column) {
		if (row == "A") {
			i = 0 ;
		}
		
		else if (row == "B") {
			i = 1 ;
		}
		
		else if (row == "C") {
			i = 2 ;
		}
		
		else if (row == "D") {
			i = 3 ;
		}
		
		else if (row == "E") {
			i = 4 ;
		}
		
		else if (row == "F") {
			i = 5 ;
		}
		
		else if (row == "G") {
			i = 6 ;
		}
		
		else if (row == "H") {
			i = 7 ;
		}
		
		else if (row == "I") {
			i = 8 ;
		}
		
		else if (row == "J") {
			i = 9 ;
		}
		
		else if (row == "K") {
			i = 10 ;
		}
		
		else if (row == "L") {
			i = 11 ;
		}
		
		else if (row == "M") {
			i = 12 ;
		}
		
		else if (row == "N") {
			i = 13 ;
		}
		
		else if (row == "O") {
			i = 14 ;
		}
		
		j = column - 1 ; 
		
		buyTicket(i,j) ;
	}
	
	public void buyTicket(int i, int j) {
		price = ticket[i][j] ;
		
		if (price == 0) {
			p = "Sorry, this ticket is sold out." ;
		}
		
		else if (price != 0) {
			p = Integer.toString(price) + " Bath." ;
		}
		
		setPrice(i,j) ;
		
	}
	
	public String getPrice() {
		return p ;
	}
	
	public void setPrice(int i, int j) {
		ticket[i][j] = 0 ;
	}
	
}
